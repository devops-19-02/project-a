image: alpine:latest

stages:
  - build
  - test
  - review
  - release
  - cleanup
  - staging
  - production

build:
  stage: build
  image: docker:git
  services:
    - docker:dind
  script:
    - setup_docker
    - docker_login
    - build
  variables:
    DOCKER_DRIVER: overlay2
  only:
    - branches

test:
  stage: test
  image: docker:git
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2  
  script:
    - setup_docker
    - docker_login
    - test_ui
    - test_crawler
  only:
    - branches

release:
  stage: release
  image: docker
  services:
    - docker:dind
  script:
    - setup_docker
    - docker_login
    - release
  only:
    - master

review:
  stage: review
  script:
    - install_dependencies
    - kube_config
    - ensure_namespace
    - registry_credentials
    - install_tiller
    - deploy_review
  variables:
    KUBE_NAMESPACE: review
    host: $CI_COMMIT_REF_SLUG.dev.tennki.tk
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_COMMIT_REF_SLUG.dev.tennki.tk
    on_stop: stop_review
  only:
    refs:
      - branches
  except:
    - master

stop_review:
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
    - install_dependencies
    - kube_config
    - delete_review
  variables:
    host: $CI_COMMIT_REF_SLUG.dev.tennki.tk
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  when: manual
  allow_failure: true
  only:
    refs:
      - branches
  except:
    - master

staging:
  stage: staging
  script:
    - install_dependencies
    - kube_config
    - ensure_namespace
    - registry_credentials
    - install_tiller
    - deploy
  variables:
    KUBE_NAMESPACE: staging
    host: staging.tennki.tk
  environment:
    name: staging
    url: http://staging.tennki.tk
  only:
    refs:
      - master

production:
  stage: production
  script:
    - install_dependencies
    - kube_config
    - ensure_namespace
    - registry_credentials
    - install_tiller
    - deploy
  variables:
    KUBE_NAMESPACE: production
    host: production.tennki.tk
  environment:
    name: production
    url: http://production.tennki.tk
  when: manual
  only:
    refs:
      - master

.auto_devops: &auto_devops |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x
  export CI_APPLICATION_REPOSITORY=$CI_REGISTRY/$CI_PROJECT_PATH
  export CI_APPLICATION_TAG=$CI_COMMIT_REF_SLUG
  export CI_CONTAINER_NAME=ci_job_build_${CI_JOB_ID}
  export TILLER_NAMESPACE="kube-system"

  function build() {
    
    env
    echo "Building Dockerfile-based application..."
    echo `git show --format="%h" HEAD | head -1` > build_info.txt
    echo `git rev-parse --abbrev-ref HEAD` >> build_info.txt
    docker build -t "$CI_APPLICATION_REPOSITORY/se-ui:$CI_APPLICATION_TAG" ./source/se-ui/
    docker build -t "$CI_APPLICATION_REPOSITORY/se-crawler:$CI_APPLICATION_TAG" ./source/se-crawler/

    echo "Pushing to GitLab Container Registry..."
    docker push "$CI_APPLICATION_REPOSITORY/se-ui:$CI_APPLICATION_TAG"
    docker push "$CI_APPLICATION_REPOSITORY/se-crawler:$CI_APPLICATION_TAG"
    echo ""
  }

  function test_ui() {
    docker run --rm -w /app $CI_REGISTRY_IMAGE/se-ui:$CI_APPLICATION_TAG python3 -m unittest discover -s /app/tests/
  }
  
  function test_crawler() {
    docker run --rm -w /app $CI_REGISTRY_IMAGE/se-ui:$CI_APPLICATION_TAG python3 -m unittest discover -s /app/tests/
  }

  function release() {

    echo "Updating docker images ..."
    docker pull "$CI_APPLICATION_REPOSITORY/se-ui:$CI_APPLICATION_TAG"
    docker pull "$CI_APPLICATION_REPOSITORY/se-crawler:$CI_APPLICATION_TAG"
    
    docker tag "$CI_APPLICATION_REPOSITORY/se-ui:$CI_APPLICATION_TAG" "$CI_APPLICATION_REPOSITORY/se-ui:$(cat source/se-ui/VERSION)"
    docker tag "$CI_APPLICATION_REPOSITORY/se-crawler:$CI_APPLICATION_TAG" "$CI_APPLICATION_REPOSITORY/se-crawler:$(cat source/se-crawler/VERSION)"
    
    docker push "$CI_APPLICATION_REPOSITORY/se-ui:$(cat source/se-ui/VERSION)"
    docker push "$CI_APPLICATION_REPOSITORY/se-crawler:$(cat source/se-crawler/VERSION)"
    echo ""
  }

  function docker_login() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      docker login $CI_REGISTRY_IMAGE -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
      echo ""
    fi
  }

  function install_dependencies() {

    apk add -U openssl curl tar gzip bash ca-certificates git python
    wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.23-r3/glibc-2.23-r3.apk
    apk add glibc-2.23-r3.apk
    rm glibc-2.23-r3.apk

    curl https://storage.googleapis.com/kubernetes-helm/helm-v2.14.1-linux-amd64.tar.gz | tar zx

    mv linux-amd64/helm /usr/bin/
    helm version --client

    curl -L -o /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x /usr/bin/kubectl
    kubectl version --client

    cd /
    export PATH=/google-cloud-sdk/bin:$PATH
    curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-251.0.0-linux-x86_64.tar.gz
    tar xzf google-cloud-sdk-251.0.0-linux-x86_64.tar.gz
    rm google-cloud-sdk-251.0.0-linux-x86_64.tar.gz
    ln -s /lib /lib64
    gcloud config set core/disable_usage_reporting true
    gcloud config set component_manager/disable_update_check true
    gcloud --version

  }

  function ensure_namespace() {

    kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"


  }

  function registry_credentials() {

    kubectl get secret regcred -n "$KUBE_NAMESPACE" || kubectl create secret docker-registry regcred --docker-server=$CI_REGISTRY_IMAGE --docker-username=$CI_DEPLOY_USER --docker-password=$CI_DEPLOY_PASSWORD -n "$KUBE_NAMESPACE"
    
  }
  
  function kube_config() {

    echo "$GITLAB_SA_KEY" > key.json
    gcloud auth activate-service-account --key-file=key.json
    gcloud container clusters get-credentials $GCP_KE_CLUSTER --zone $GCP_ZONE --project $GCP_PROJECT
    gcloud config set project $GCP_PROJECT

  }


  function deploy_review() {
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    echo "Add DNS Record..."    
    gcloud dns record-sets transaction start -z=$GCP_DNS_ZONE_NAME  
    gcloud dns record-sets transaction add -z=$GCP_DNS_ZONE_NAME --type=CNAME --name="$host." --ttl 600 "production.tennki.tk."
    gcloud dns record-sets transaction execute -z=$GCP_DNS_ZONE_NAME

    cd $CI_PROJECT_DIR

    echo "Download helm dependencies..."
    helm dep update deploy/charts/se

    echo "Deploy helm release $name to $KUBE_NAMESPACE namespace"
    helm upgrade --install \
      --wait \
      --set se-ui.ingress.host="$host" \
      --set se-ui.image.repository=$CI_REGISTRY_IMAGE/se-ui \
      --set se-ui.image.tag=$CI_APPLICATION_TAG \
      --set se-crawler.image.repository=$CI_REGISTRY_IMAGE/se-crawler \
      --set se-crawler.image.tag=$CI_APPLICATION_TAG \
      --namespace="$KUBE_NAMESPACE" \
      --version="$CI_PIPELINE_ID-$CI_JOB_ID" \
      "$name" \
      deploy/charts/se/
  }

  function deploy() {
    echo $KUBE_NAMESPACE
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"

    cd $CI_PROJECT_DIR
    echo "Download helm dependencies..."
    helm dep build deploy/charts/se

    echo "Deploy helm release $name to $KUBE_NAMESPACE namespace"
    helm upgrade --install \
      --wait \
      --set se-ui.ingress.host="$host" \
      --set se-ui.ingress.clusterissuer="$PROD_CERT_ISSUER" \
      --set se-ui.image.repository=$CI_REGISTRY_IMAGE/se-ui \
      --set se-ui.image.tag="$(curl https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/raw/master/source/se-ui/VERSION)" \
      --set se-crawler.image.repository=$CI_REGISTRY_IMAGE/se-crawler \
      --set se-crawler.image.tag="$(curl https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/raw/master/source/se-crawler/VERSION)" \
      -f deploy/charts/se/values-production.yaml \
      --namespace="$KUBE_NAMESPACE" \
      --version="$CI_PIPELINE_ID-$CI_JOB_ID" \
      "$name" \
      deploy/charts/se/
  }

  function install_tiller() {
    echo "Checking Tiller..."
    helm init --upgrade
    kubectl rollout status -n "$TILLER_NAMESPACE" -w "deployment/tiller-deploy"
    if ! helm version --debug; then
      echo "Failed to init Tiller."
      return 1
    fi
    echo ""
  }

  function delete_review() {
    track="${1-stable}"
    name="$CI_ENVIRONMENT_SLUG"

    echo "Remove $name release..."
    helm delete "$name" --purge || true
    
    echo "Remove DNS Record..."
    gcloud dns record-sets transaction start -z=$GCP_DNS_ZONE_NAME  
    gcloud dns record-sets transaction remove -z=$GCP_DNS_ZONE_NAME --type=CNAME --name="$host." --ttl 600 "production.tennki.tk."
    gcloud dns record-sets transaction execute -z=$GCP_DNS_ZONE_NAME

  }

  function setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

before_script:
  - *auto_devops
